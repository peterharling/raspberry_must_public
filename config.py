# mariadb credentials
DB_USER = "root"
DB_HOST = "localhost"
DB_PORT = 3306
DB_NAME = "inverter"
DB_PASSWORD = "**********"

# mqtt server credentials
MQTT_HOST = "localhost"
MQTT_PORT = 1883
MQTT_KEEP_ALIVE = 60
MQTT_TOPIC = "inverter"

# open weather api credentials
OW_URL = "http://api.openweathermap.org/data/2.5/weather?"
OW_KEY = "**********"
OW_CITY = "276781"

# influxdb api credentials
INFLUX_URL = "https://europe-west1-1.gcp.cloud2.influxdata.com"
INFLUX_KEY = "**********"
INFLUX_ORG = "pharling.cg@gmail.com"
INFLUX_BUCKET = "pharling.cg's Bucket"

# inverter connection settings
SERIAL_PORT = "/dev/ttyUSB0"
SERIAL_TIMEOUT = 0.5
SERIAL_BAUD = 19200
SERIAL_SLAVE = 4

# mail credentials
MAIL_SENDER = "**********"
MAIL_PASSWORD = "**********"
MAIL_RECEIVER = "pharling.cg@gmail.com"
SMTP_SERVER = "smtp.gmail.com"
SMTP_PORT = 587

# google cloud credentials
GOOGLE_PROJECT = "RPi solar"
GOOGLE_KEY = "**********"
GOOGLE_CLIENT = "RPi solar"
GOOGLE_ID = "**********"
GOOGLE_SECRET = "**********"

# miscellaneous
DATE_FORMAT = "%d/%m/%Y %H:%M:%S.%f"
