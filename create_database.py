import mariadb
import config

# queries that will be passed below to the MYSQL/MariaDB server
query_create_db = f"CREATE OR REPLACE DATABASE {config.DB_NAME}"
query_create_table = """CREATE TABLE `inverter_data` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `Timestamp` timestamp NOT NULL,
        `Weather` varchar(100),
        `Work state` varchar(25),
        `PV voltage` float,
        `Charger current` float,
        `PV power` float,
        `Battery voltage` float,
        `Inverter voltage` float,
        `Grid voltage` float,
        `Inverter current` float,
        `Grid current` float,
        `Load current` float,
        `Inverter power` float,
        `Grid power` float,
        `Load power` float,
        `Load percent` float,
        `Inverter frequency` float,
        `Grid frequency` float,
        `Battery power` float,
        `Battery current` float,
        PRIMARY KEY(`id`)
        )"""

# create database on MariaDB server
try:
    print("Attempting to create database...")
    conn = mariadb.connect(user=config.DB_USER, 
            password=config.DB_PASSWORD, 
            host=config.DB_HOST, 
            port=config.DB_PORT)
    cursor = conn.cursor()
    cursor.execute(query_create_db)
    conn.commit()
    conn.close()
    print("Successfully created database!")
except mariadb.Error as e:
    print(e)

# create table within above database
try:
    print("Attempting to create table...")
    conn = mariadb.connect(user=config.DB_USER, 
            password=config.DB_PASSWORD, 
            host=config.DB_HOST, 
            port=config.DB_PORT,
            database=config.DB_NAME)
    cursor = conn.cursor()
    cursor.execute(query_create_table)
    conn.commit()
    conn.close()
    print("Successfully created table!")
except mariadb.Error as e:
    print(e)
