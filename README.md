<h1>Solar system monitoring</h1>

<p>This is a solution to extract, log, broadcast, and visualize data from a MUST inverter at the heart of a solar power system.</p> 

<h2>The problem</h2>

<p>This MUST inverter, like many other inverters, collects an enormous amount of useful data on your solar power system, but only gives you access to a fraction of it, via a poorly designed application. The inverter comes with a WiFi dongle that connects to your internet network to broadcast your data to the company's server. It also comes with rudimentary desktop and mobile apps that give you a general sense of your production and consumption of electricity, but eclipses the wealth of data sent to the server.</p>

<p>The MUST inverter is a 5kW, hybrid (on/off grid) inverter from the <a href="https://www.mustpower.com/ph1800-mpk-plus-series-high-frequency-onoff-grid-hybrid-solar-inverter-1-5kva/">PH1800 Plus series</a>.</p>

<a data-flickr-embed="true" href="https://www.flickr.com/photos/145037197@N08/52362433367/in/dateposted/" title="solar_system"><img src="https://live.staticflickr.com/65535/52362433367_a00e96409d_h.jpg" max-width="100%" height="auto" alt="solar_system"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<p>Electricity-related data can also be extremely granular and intimate: It can provide insights into your household's size, routines, income, and even life-cycles (such as a birth or phase of unemployment). This raises interesting privacy concerns, were the data to be used for unregulated commercial purposes.</p>

<h2>Solution overview</h2>

<p>This solution runs on a Raspberry Pi connected to the inverter's USB communication port via an RS485 cable. The RPi reads the inverter's registers every 30s. If the checksum is verified, it then logs the data locally, broadcasts it over MQTT, and uploads the data to InfluxDB Cloud. From there, it can be visualized in Grafana. The inverter itself can therefore be disconnected from the internet and the company's server.</p>

<a data-flickr-embed="true" href="https://www.flickr.com/photos/145037197@N08/52363785175/in/photostream/" title="solar_dashboard"><img src="https://live.staticflickr.com/65535/52363785175_ca8cc2ec6c_h.jpg" max-width="100%" height="auto" alt="solar_dashboard"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<h2>Hardware</h2>

<ul>
<li>Raspberry Pi 4 with 2GB memory, if nothing else is running on it</li>
<li>Aluminum enclosure doubling up as a heatsink, given high temperatures and dust in the inverter cubbyhole</li>
<li>Mini UPS to keep the RPi running even during overload incidents or a shutdown</li>
<li>RS485 cable, provided with the inverter</li>
<li>SD card with 32GB storage, to allow for Raspbian and logging large amounts of data over several years</li>
</ul>

<h2>Obstacles to overcome</h2>

<p>This project raised a variety of obstacles, which can be listed as follows, along with their solutions.</p>

<ul>
<li>Finding the serial communication protocol used by the inverter, which likely happens to be a mainstream choice, ModBus</li>
<li>Error-prone serial communication with the inverter, which makes it necessary not just to verify the checksum, but to use a good quality cable, suited to an environment full of electrical interferences</li>
<li>API requests that tend to hang, not least because of unstable internet, which is why logging to a local database is detached from broadcasting to the cloud, with an MQTT connection between two scripts running in parallel.</li>
<li>Unexpected and unreported corruption of the MariaDB almost a year into the logging process, which called for a safe backup, thus the emails sent out every week to never loose more than a few days' data</li>
</ul>

<h2>Credits</h2>

<p>This project benefited enormously from the research available <a href="https://aquarat.co.za/teardowns/reverse-engineering-a-solar-inverter-telemetry-protocol/">here.</a></p>

<p>One of the hardest parts of reading from an inverter is to map the Modbus registers to read from, whose addresses are rarely disclosed by the suppliers. Thankfully, this MUST inverter has been mapped <a href="https://github.com/dylangmiles/docker-must-homeassistant/blob/main/Ph1800.json">here.</p>