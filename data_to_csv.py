import csv
import mariadb
import config

# state the csv file headers
column_names = ['id', 'Timestamp', 'Weather', 'Work state', 
        'PV voltage (V)', 'Charger current (A)', 'PV power (W)', 
        'Battery voltage (V)', 'Inverter voltage (V)', 'Grid voltage (V)', 
        'Inverter current (A)', 'Grid current (A)', 'Load current (A)', 
        'Inverter power (W)', 'Grid power (W)', 'Load power (W)', 
        'Load percent (%)', 'Inverter frequency (Hz)', 'Grid frequency (Hz)', 
        'Battery power (W)', 'Battery current (A)']

# connect to MySQL/MariaDB server
connection = mariadb.connect(
        host=config.DB_HOST,
        user=config.DB_USER, 
        password=config.DB_PASSWORD,
        database=config.DB_NAME) 

# create a cursor to pass on commands to MySQL/MariaDB
cursor = connection.cursor() 
 
# open the file in write mode and write first row
with open(r'inverter_data_dump','w') as f: 
    writer = csv.writer(f)
    writer.writerow(column_names)
 
# execute the SQL command in MySQL/MariaDB to collect all the data in the DB
cursor.execute('''SELECT * FROM inverter_data;''') 
 
# write all the data collected into the csv file, by opening it in append mode
for row in cursor.fetchall(): 
    with open(r'inverter_data_dump', 'a') as f:
        writer = csv.writer(f)
        writer.writerow([row])

# close connection and clean up
connection.close()