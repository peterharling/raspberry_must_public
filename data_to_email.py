# this script still needs testing and is not yet fully operational
import time
import csv
import mariadb
import schedule
import smtplib, ssl
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
import config

# define the CSV file name
csv_name = r"inverter_data_week"

# define the email content
email_subject = "Last week's solar system data"
email_body = """Hello,
Please find attached, as a backup, the data collected by the solar system last week.
"""

# define the SQL query to get all the data from the week that precedes
query_last_week = """SELECT * FROM INVERTER_DATA WHERE WEEK(Timestamp)=WEEK(NOW())-1;"""

# state the csv file headers
column_names = ["id", "Timestamp", "Weather", "Work state", 
        "PV voltage (V)", "Charger current (A)", "PV power (W)", 
        "Battery voltage (V)", "Inverter voltage (V)", "Grid voltage (V)", 
        "Inverter current (A)", "Grid current (A)", "Load current (A)", 
        "Inverter power (W)", "Grid power (W)", "Load power (W)", 
        "Load percent (%)", "Inverter frequency (Hz)", "Grid frequency (Hz)", 
        "Battery power (W)", "Battery current (A)"]

# collect last week"s data from the MySQL/MariaDB server and write to a csv file
def fetch_data():
    # connect to MySQL/MariaDB server
    connection = mariadb.connect(
            host=config.DB_HOST,
            user=config.DB_USER, 
            password=config.DB_PASSWORD,
            database=config.DB_NAME) 
    # create a cursor to pass on commands to MySQL/MariaDB
    cursor = connection.cursor() 
    # open the file in write mode and write first row
    with open(csv_name,"w") as f: 
        writer = csv.writer(f)
        writer.writerow(column_names)
    # execute the SQL command in MySQL/MariaDB to collect all the data in the DB
    cursor.execute(query_last_week)  
    # write all the data collected into the csv file, by opening it in append mode
    for row in cursor.fetchall(): 
        with open(csv_name, "a") as f:
            writer = csv.writer(f)
            writer.writerow([row])
    # close connection and clean up
    connection.close()

# send the csv file containing the data by email 
def email_data():
    # create a secure SSL context
    context = ssl.create_default_context()
    # create a multipart message and set headers
    message = MIMEMultipart()
    message["From"] = config.MAIL_SENDER
    message["To"] = config.MAIL_RECEIVER
    message["Subject"] = email_subject
    # add body to email
    message.attach(MIMEText(email_body, "plain"))
    # open and read the csv file in binary, and attach it to the email
    with open(csv_name, 'rb') as attachment:
        message.attach(MIMEApplication(attachment.read(), Name=csv_name))
    # create a secure smtp connection, login, and send email 
    try:
        smtpserver = smtplib.SMTP(config.SMTP_SERVER, config.SMTP_PORT)
        smtpserver.starttls(context=context)
        smtpserver.login(config.MAIL_SENDER, config.MAIL_PASSWORD)
        smtpserver.sendmail(config.MAIL_SENDER, config.MAIL_RECEIVER, message.as_string())
    except Exception as error:
        print("Couldn't send email: " + str(error))
    finally:
        smtpserver.quit()

# schedule actions on a weekly basis 
# ideally, add a cron job and sys.exit() to avoid having this script running all week for nothing
schedule.every().monday.at("08:00").do(fetch_data())
schedule.every().monday.at("08:10").do(email_data())

while True:
    schedule.run_pending()
    time.sleep(1)
