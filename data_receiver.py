import paho.mqtt.client as mqtt
import fhem
import json
import time
import urllib3
import influxdb_client
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
from datetime import datetime
import config

date_format = '%d/%m/%Y %H:%M:%S.%f'

token = config.INFLUX_KEY
org = config.INFLUX_ORG
bucket = config.INFLUX_BUCKET

# fetch and format time
def get_timestamp():
    dt = datetime.now()
    return dt.strftime(date_format)

# send a selection of the data received via mqtt to influxdb
def upload_to_influxdb(data):
    try:
        client = InfluxDBClient(url='https://europe-west1-1.gcp.cloud2.influxdata.com', token=token, org=org, timeout=5000)
        write_api = client.write_api(write_options=SYNCHRONOUS)
        _datapoint1 = Point("inverter data").tag("device", "rpi").field("Load current", data["Load current"])
        _datapoint2 = Point("inverter data").tag("device", "rpi").field("PV power", data["PV power"])
        _datapoint3 = Point("inverter data").tag("device", "rpi").field("Battery voltage", data["Battery voltage"])
        _datapoint4 = Point("inverter data").tag("device", "rpi").field("Inverter voltage", data["Inverter voltage"])
        _datapoint5 = Point("inverter data").tag("device", "rpi").field("Inverter frequency", data["Inverter frequency"])
        _datapoint6 = Point("inverter data").tag("device", "rpi").field("Battery current", data["Battery current"])
        _datapoint7 = Point("inverter data").tag("device", "rpi").field("Inverter power", data["Inverter power"])
        _datapoint8 = Point("inverter data").tag("device", "rpi").field("Grid frequency", data["Grid frequency"])
        _datapoint9 = Point("inverter data").tag("device", "rpi").field("Grid voltage", data["Grid voltage"])
        write_api.write(bucket=bucket, record=[_datapoint1, _datapoint2, _datapoint3, _datapoint4, _datapoint5, _datapoint6, _datapoint7, _datapoint8, _datapoint9])
        client.close()
    except Exception as error:
        print("Failed to upload to InfluxDB... " + str(error))

# subscribe to the relevant mqtt topic
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        client.connected_flag = True
        print("Connected with result code: " + str(rc))
        client.subscribe("inverter")
    else:
        client.bad_connection_flag = True
        print("Bad connection with result code: " + str(rc))

# detect disconnection from the mqtt broker
def on_disconnect(client, userdata, rc):
    logging.info("Disconnecting for reason: " + str(rc))
    client.connected_flag = False
    client.disconnect_flag = True

# handle the data received via mqtt
def on_message(client, userdata, msg):
    data = json.loads(msg.payload)
    print(data)
    try:
        upload_to_influxdb(data)
        print("Data uploaded to InfluxDB!")
    except Exception as error:
        print("Failed to upload data to InfluxDB..." + str(error))
    # examples of potential triggers within the data
    if b"clouds" in msg.payload:
        print("Cloudy weather detected!")
    if data["Load current"] > 10:
        print("Consumption above 5A detected")

mqtt.Client.connected_flag = False
mqtt.Client.bad_connection_flag = False
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect(host="localhost", port=1883, keepalive=60)
"""
while not client.connected_flag and not client.bad_connection_flag:
    print("Waiting to connect to mqtt...")
    time.sleep(2)
"""
client.loop_forever()
