import sys
import subprocess

"""
In addition to the packages below:

1) update and upgrade
- sudo apt update
- sudo apt upgrade

1) install and check the mariadb server
- sudo apt install mariadb-server
- sudo mysql_secure_installation
- sudo mariadb -u root -p

2) install and start the mosquitto broker
- sudo apt-get install mosquitto
- sudo apt-get install mosquitto-clients
- sudo service mosquitto stop
- mosquitto -v
"""

# list all packages required to run other scripts
packages = [
        "requests", 
        "urllib3", 
        "minimalmodbus", 
        "mariadb", 
        "paho-mqtt", 
        "fhem", 
        "schedule",
        "influxdb-client",
        "google-api-python",
        "google-auth-httplib2",
        "google-auth-oauthlib"
        ]

# install via pia each package included in the list
for package in packages:
    try:
        print(f"Attempting to install {package}...")
        subprocess.check_call([sys.executable, "-m", "pip", "install", f"{package}"])
    except Exception as e:
        print(f"Failed to install {package}: " + e)