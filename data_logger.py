import os, sys, io
import mariadb
import requests
import minimalmodbus
import paho.mqtt.client as mqtt
import json
from time import sleep
from datetime import datetime
import config

# mariadb credentials
user = config.DB_USER
password = config.DB_PASSWORD
query = "INSERT INTO inverter_data (Timestamp, Weather, `Work state`, `PV voltage`, `Charger current`, `PV power`, `Battery voltage`, `Inverter voltage`, `Grid voltage`, `Inverter current`, `Grid current`, `Load current`, `Inverter power`, `Grid power`, `Load power`, `Load percent`, `Inverter frequency`, `Grid frequency`, `Battery power`, `Battery current`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

# open weather api credentials
KEY = config.OW_KEY
URL = config.OW_URL
city = config.OW_CITY
request = URL + "appid=" + KEY + "&id=" + city

# inverter connection settings
SERIAL_PORT = config.SERIAL_PORT
SERIAL_TIMEOUT = config.SERIAL_TIMEOUT
SERIAL_BAUD = config.SERIAL_BAUD

# list the known registers recording data on the inverter, and comment out those to ignore
register_map = {
    15205 : ["PV voltage", 0.1, "V"],                   # 0
    #15206 : ["Battery voltage", 0.1, "V"],             # 1
    15207 : ["Charger current", 0.1, "A"],              # 2
    15208 : ["PV power", 1, "W"],                       # 3
    25201 : ["Work state", 1, "map", {                  # 4
        0 : "PowerOn",
        1 : "SelfTest",
        2 : "OffGrid",
        3 : "Grid-Tie",
        4 : "Bypass",
        5 : "Stop",
        6 : "Grid Charging"}],
    25205 : ["Battery voltage", 0.1, "V"],              # 8
    25206 : ["Inverter voltage", 0.1, "V"],             # 9
    25207 : ["Grid voltage", 0.1, "V"],                 # 10
    #25208 : ["BUS voltage", 0.1, "V"],                 # 11
    #25209 : ["Control current", 0.1, "A"],             # 12
    25210 : ["Inverter current", 0.1, "A"],             # 13
    25211 : ["Grid current", 0.1, "A"],                 # 14
    25212 : ["Load current", 0.1, "A"],                 # 15
    25213 : ["Inverter power", 1, "W"],                 # 16
    25214 : ["Grid power", 1, "W"],                     # 17
    25215 : ["Load power", 1, "W"],                     # 18
    25216 : ["Load percent", 1, "%"],                   # 19
    #25217 : ["Inverter complex power(S)", 1, "VA"],    # 20
    #25218 : ["Grid complex power(S)", 1, "VA"],        # 21
    #25219 : ["Load complex power(S)", 1, "VA"],        # 22
    #25221 : ["Inverter reactive power(Q)", 1, "var"],  # 24
    #25222 : ["Grid reactive power(Q)", 1, "var"],      # 25
    #25223 : ["Load reactive power(Q)", 1, "var"],      # 26
    25225 : ["Inverter frequency", 0.01, "Hz"],         # 28
    25226 : ["Grid frequency", 0.01, "Hz"],             # 29
    #25233 : ["AC radiator temperature", 1, "C"],       # 36
    #25234 : ["Transformer temperature", 1, "C"],       # 37
    #25235 : ["DC radiator temperature", 1, "C"],       # 38
    #25254 : ["Accumulated load power", 0.1, "kWh"],    # 57
    #25256 : ["Accumulated self-use power", 0.1, "kWh"],# 59
    25273 : ["Battery power", 1, "W"],                  # 76
    25274 : ["Battery current", 1, "A"]                 # 77
}

# define the registers that will be queried
registers_used = [15205, 15207, 15208, 25201, 25205, 25206, 25207, 25210, 25211, 25212, 25213, 25214, 25215, 25216, 25225, 25226, 25273, 25274]
registers_index = [0, 2, 3, 4, 8, 9, 10, 13, 14, 15, 16, 17, 18, 19, 28, 29, 76, 77]

# initialize the variables receiving the data
results_1 = 0
results_2 = 0
datapoint = {}

# fetch and format the time
def get_timestamp():
    print("Attempting to get timestamp.")
    try:
        dt = datetime.now()
        return dt.strftime("%Y-%m-%d %H:%M:%S")
    except:
        print("Failed to get timestamp.")
        return "no response"

# fetch the weather from open weather api
def get_weather(): 
    print("Attempting to get weather.")
    try:
        weather_data = requests.get(request, timeout=5).json()
        weather_description = weather_data["weather"][0]["description"]
        return weather_description
    except: 
        print("Failed to get weather")
        weather_description = "no response"
        return weather_description

# read the first set of registers on the inverter
def read_registers_1(inverter, startreg, count):
    return inverter.read_registers(startreg, count)

# read the second set of registers on the inverter
def read_registers_2(inverter, startreg, count):
    return inverter.read_registers(startreg, count)

# record the inverter data locally on the MySQL/MariaDB server
def log_data(datapoint):
    try:
        print("Attempting to connect to MariaDB.")
        conn = mariadb.connect(user=user, password=password, host="localhost", port=3306, database="inverter")
        cursor = conn.cursor()
        print("Attempting to write to MariaDB.")
        cursor.execute(query, (datapoint["Timestamp"], datapoint["Weather"], datapoint["Work state"], datapoint["PV voltage"], datapoint["Charger current"], datapoint["PV power"], datapoint["Battery voltage"], datapoint["Inverter voltage"], datapoint["Grid voltage"], datapoint["Inverter current"], datapoint["Grid current"], datapoint["Load current"], datapoint["Inverter power"], datapoint["Grid power"], datapoint["Load power"], datapoint["Load percent"], datapoint["Inverter frequency"], datapoint["Grid frequency"], datapoint["Battery power"], datapoint["Battery current"]))
        conn.commit()
        conn.close()
    except mariadb.Error as error:
        print("Failed to log data: " + error)

# publish the data via MQTT, to handle it via other scripts or on other devices
def broadcast_data(datapoint):
    try:
        client = mqtt.Client()
        client.connect("localhost", 1883, 60)
        # changing the data type from json to a dictionary in string format, compatible with mqtt
        data = json.dumps(datapoint) 
        client.publish(topic="inverter", payload=data, qos=1)
        print("Broadcasted data via mqtt.")
    except Exception as error:
        print("Failed to broadcast data: " + str(error))

# loop every 30s to read the inverter data, format it, log it, and broadcast it
while True:
    
    # initialize the inverter connection, with port name and slave address in decimal
    try: 
        inverter = minimalmodbus.Instrument(SERIAL_PORT, 4) 
        inverter.serial.baudrate = SERIAL_BAUD
        inverter.serial.timeout  = SERIAL_TIMEOUT
        sleep(1)
    except:
        print("Inverter not found.")
    
    # read data from inverter and verify checksum
    if results_1 == 0:
        try:
            results_1 = read_registers_1(inverter, 15205, 4) 
            print("Successfully read registers 1.")                
        except Exception as error:
            print("Inaccurate response... retrying. " + str(error))
    
    # read data from inverter and verify checksum
    if results_2 == 0:
        try:
            results_2 = read_registers_2(inverter, 25201, 74)
            print("Successfully read registers 2.")
        except Exception as error:
            print("Inaccurate response... retrying. " + str(error))
    
    # compile data, add timestamp and weather, and format as a dictionary using the regiser map above
    if results_1 != 0 and results_2 != 0:
        results = results_1 + results_2
        datapoint["Timestamp"] = get_timestamp()
        datapoint["Weather"] = get_weather()
        print("Attempting to extract values.")

        for i in range(len(registers_index)):
            index = registers_index[i]
            register = registers_used[i]
            register_name = register_map[register][0]
            if register_map[register][2] == "map":
                register_value = register_map[register][3][results[index]]
            else:
                register_value = float(results[index] * register_map[register][1])
                # ensure negative values are logged as such, as the inverter data is unsigned
                if register_value > 32768:
                    register_value = register_value - 65536 
            datapoint[register_name] = register_value

        print("Attempting to log data.")
        log_data(datapoint)
        broadcast_data(datapoint)
        print("Wrote data at " + str(datapoint["Timestamp"]))
        print(datapoint)
        # reset inverter query results and start a new loop after 30s
        results_1 = 0
        results_2 = 0
        sleep(30)
